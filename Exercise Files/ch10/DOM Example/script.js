//DOM
/*
Document - can be represented by the HTML or a 
			screenshot of the browser rendered web-page
		 - "The Page"
		 *web page*
Objects - Different elements or components of the document
		- i.e. <h1>, <ul>, certain div, etc.
		*pieces*
Model - what do we call individual pieces/how do we describe 
		relationship between them?
	   - *agreed upon set of terms*
*/

var headline = document.getElementById("mainHeading");

headline.innerHTML = "Wow, a new headline!";
